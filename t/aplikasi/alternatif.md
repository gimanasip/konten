---
judul: "Aplikasi Alternatif"
deskripsi: "Belum bisa bayar Photoshop, CorelDraw, Illustrator, Premiere, Office Word, Excel, dan PowerPoint? Aplikasi atau *software* berikut ini mungkin bisa jadi pilihan."
status: "rancangan"
label: 
  - Aplikasi
resensi: 
  - Aplikasi
tanggal: "2021-04-08T13:31:03Z"
lastmod: ‎"2021-05-27T20:47:29Z"
---

*Vice versa*; perkembangan internet mempengaruhi perkembangan perangkat lunak. *Software* yang dulu tidak mudah diusahakan, kini menjadi mungkin untuk dikembangkan secara kolaboratif. Aplikasi yang dibuat dari perusahaan, belum tentu lebih baik dari yang dikembangkan bersama-sama. 

[*Ala* bisa, karena biasa](:kutip)

Apa saja daftar aplikasi dengan kegunaan yang sejenis?

## Dokumen Kantor atau *Office* {.x}
Secara umum, administrasi kantor membutuhkan pengolah teks, tabel, presentasi. 

### Teks {.v}
Pengolah tulisan biasa digunakan untuk membuat surat atau dokumen administrasi lain.

[Office Word](/a/com/microsoft/office/word.md "Perangkat lunak pembuatan dokumen dan pengolahan teks")
[Google Docs](/a/com/google/docs.md "Situs aplikasi pembuatan dokumen dan pengolahan teks")
[LibreOffice Writer](/a/org/libreoffice/writer.md "Sering disebut sebagai penerus OpenOffice. Pengembangan OpenOffice berhenti sejak tahun 2011.")  
[Collabora Office Text](/a/com/collabora/libreoffice.md "Pengolah dokumen untuk Android dan iOS berbasis LibreOffice.")

{{< v >}}
### Tabel dan Data {.v}
Pengolah data biasa digunakan untuk mengolah dan menyajikan data administratif, keuangan, atau lainnya.

[Office Excel](/a/com/microsoft/office/excel.md "Perangkat lunak pembuatan tabel dan pengolahan data")
[Google Sheets](/a/com/google/sheets.md "Situs aplikasi pembuatan tabel dan pengolahan data")
[LibreOffice Calc](/a/org/libreoffice/calc.md "Perangkat lunak pembuatan tabel dan pengolahan data")
[Collabora Office Spreadsheets](/a/com/collabora/libreoffice.md "Pengolah dokumen untuk Android dan iOS berbasis LibreOffice.")

{{< v >}}
### Presentasi {.v}
Dalam pertemuan, penyajian gagasan dapat menggunakan perangkat berikut.

[Office PowerPoint](/a/com/microsoft/office/powerpoint.md "Perangkat lunak presentasi slide")
[Google Slides](/a/com/google/slides.md "Situs aplikasi presentasi slide")
[LibreOffice Impress](/a/org/libreoffice/impress.md "Perangkat lunak presentasi slide")
[Collabora Office Presentations](/a/com/collabora/libreoffice.md "Pengolah dokumen untuk Android dan iOS berbasis LibreOffice.")

{{< v >}}

+ WPS Office
+ SmartOffice

![Pengolahan tulisan, data, maupun visualisasi untuk kebutuhan lebih khusus bisa dijelajah di artikel lain [^t/aplikasi/].](!! "Olah Data")

[^t/aplikasi/]: [Artikel tentang aplikasi](/aplikasi/)

{{< v >}}
## Grafis {.x}
### Fotografi {.v}
[Photoshop](/a/com/adobe/photoshop.md "Perangkat lunak pengolah gambar dan foto")
[GIMP](/a/org/gimp.md "Perangkat lunak pengolah gambar dan foto")
[Krita](/a/org/krita.md "Perangkat lunak pengolah gambar dan foto")
+ MyPaint
+ Paint.NET  
+ Irfanview  

+ Snapseed
+ Photo Editor
+ Phimp.me

{{< v >}}
### Vektor {.v}
[CorelDRAW](/a/com/corel/draw.md "Perangkat lunak pengolah gambar vektor")
[Illustrator](/a/com/adobe/illustrator.md "Perangkat lunak pengolah gambar vektor")
[Inkscape](/a/org/inkscape.md "Perangkat lunak pengolah gambar vektor")

{{< v >}}

Grafika digital maupun aplikasinya bisa dijelajah lanjut.

{{< v >}}
## Audio {.x}
[Audacity](/a/org/audacity.md "Perangkat lunak pengolah audio; suara dan lagu")

### DAW
[LMMS](/a/io/lmms.md "Perangkat lunak stasiun kerja audio digital; suara dan lagu")
[Ardour](/a/org/ardour.md "Perangkat lunak stasiun kerja audio digital; suara dan lagu")
+ n-Track Studio

### Musik
[MidiEditor](/a/org/midieditor.md "Perangkat lunak pengolah MIDI lagu")
[MuseScore](/a/org/musescore.md "Perangkat lunak pengolah notasi lagu")
+ Aria Maestosa

Audio digital maupun aplikasinya bisa dijelajah lanjut.

{{< v >}}
## Video {.x}
[Premiere](/a/com/adobe/premiere.md "Perangkat lunak pengolah video")
[Vegas](/a/com/magix/vegas.md "Perangkat lunak pengolah video")
[Kdenlive](/a/org/kdenlive.md "KDE Non-Linear Video Editor")
[OpenShot](/a/org/openshot.md "Perangkat lunak pengolah video")
[Shotcut](/a/org/shotcut.md "Perangkat lunak pengolah media")
[VN Video Editor](/a/me/vlognow.md "Perangkat lunak pengolah video")
+ Quik 
+ Magisto 

{{< v >}}
