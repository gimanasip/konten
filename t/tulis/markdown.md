---
date: 2019-10-31T23:59:59+07:00
description: "Teknik petanda tulisan"
featured_image: "./_gfx/image.jpg"
tags: ["Tulis", "Word", "Document"]
title: "Markdown"
nuansa: "naskah"
---

Pada era dijital kini, kebutuhan penulisan dokumen dianggap perlu kecepatan dan nilai praktis. Mencetak dokumen pada kertas hanya dilakukan pada kasus yang dianggap perlu. Seperti pada kasus keterbatasan koneksi, atau distribusi dan pengarsipan dokumen tertentu. Beberapa alasan seperti ramah lingkungan, untuk penghematan kertas, ataupun pengembangan dokumen yang dinamis, biasa menjadi alasan untuk meminimalisir dokumen cetak.

# Markdown

Pada awalnya, Markdown adalah alat konversi teks ke `HTML`. Markdown membuat kita mudah menulis juga mudah membaca teks yang kita tulis untuk format `XHTML` (juga `HTML`).

> Markdown is intended to be as easy-to-read and easy-to-write as is feasible.
> <cite>[Gruber][1]</cite>

Markdown ditulis dalam bentuk dokumen teks. Ditulis dalam jenis teks dasar agar mudah dibaca mesin maupun manusia meskipun tanpa bantuan perangakat lunak pembaca dokumen termutakhir. Selain itu ukuran dokumen relatif menjadi lebih ekonomis.

Markdown punya nilai format seperti; teks miring, tebal, kepala, kutip, taut, daftar, dll (baca [Panduan dan Sintaks Menulis Markdown](./pandu/)). Sehingga mudah disajikan secara cetak maupun dijital pada situs web. Singkatnya, Markdown mirip bentuk dokumen _office_ (seperti: `.doc`, `.docx`, `.odt`, dan _rich text_) namun berbentuk teks dasar seperti `.txt`.

Meskipun menulis Markdown bisa dilakukan menggunakan aplikasi teks dasar (seperti *notepad*). [Aplikasi untuk menulis Markdown](./aplikasi/) kini sudah cukup mudah kita dapatkan, baik untuk desktop maupun *smartphone*.

[1]:https://daringfireball.net/projects/markdown/