---
Judul: "Markdown: Panduan Menulis dan Sintaksis"
tanggal: 2019-10-31T23:59:59+07:00
deskripsi: "Cara menulis menggunakan teknik petanda Markdown"
label: ["Tulis", "Word", "Document"]
aliases:
  - /blog/menulis/markdown/
tutorial:
  - Markdown
---

## Penekanan; Tebal, Miring
Markdown memakai asterik atau tanda bintang `*` atau garis bawah `_` untuk mengapit teks miring. Sedangkan teks tebal, diapit dua tanda bintang `**` atau dua tanda garis bawah `__`. Kita tau, gaya ini sudah diadopsi aplikasi pesan seperti _WhatsApp_, *Telegram*.  
Contoh ditulis seperti berikut:

    contoh *miring*  
    atau _empasis_  
    contoh **tebal**  
    atau __penekanan__  

Pada `HTML` dibaca menjadi:

    contoh <em>miring</em>
    atau <em>empasis</em>
    contoh <strong>tebal</strong>
    atau <strong>penekanan</strong>

Akan tampil sebagai penekanan; miring dan tebal:

contoh *miring*  
atau _empasis_  
contoh **tebal**  
atau __penekanan__  

## Tautan

Dua jenis penautan bisa ditulis menggunakan Markdown yaitu: *taut-baris* dan *referensi* atau pustaka.

### Taut-baris

Teks yang ditautkan diapit tanda kurung siku `[...]` diikuti pranala yang diapit tanda kurung lengkung `(...)`.  
Contoh:

    Ini contoh [tautan](http://example.com/).

Dibaca sebagai:

    <p>Ini contoh <a href="http://example.com/">tautan</a>.</p>

Tampil seperti ini:  
Ini contoh [tautan](http://example.com/).  

Selain itu juga bisa menambahkan atribut judul tautan di dalam tanda kutip:

    Ini contoh [tautan](http://example.com/ "Beserta Judul").

Dibaca sebagai:

    <p>Ini contoh <a href="http://example.com/" title="Beserta Judul">tautan</a>.</p>

### Referensi 
Tautan sebagai daftar pustaka dapat dapat ditulis sebagai berikut:

    Sejak tahun 2009, Pedoman [Ejaan Yang Disempurnakan][EYD] diganti menjadi [Pedoman Umum Ejaan Bahasa Indonesia][PUEBI].

    [EYD]: https://id.wikipedia.org/wiki/Ejaan_yang_disempurnakan  
    [PUEBI]: https://id.wikipedia.org/wiki/Ejaan_Bahasa_Indonesia "EBI" 

Tampil menjadi:  
Sejak tahun 2009, Pedoman [Ejaan Yang Disempurnakan][EYD] diganti menjadi [Pedoman Umum Ejaan Bahasa Indonesia][PUEBI].

[EYD]: https://id.wikipedia.org/wiki/Ejaan_yang_disempurnakan  
[PUEBI]: https://id.wikipedia.org/wiki/Ejaan_Bahasa_Indonesia "EBI"  

Berbeda dengan taut-baris, pranala bisa terpisah dengan kata, frasa, atau kalimat tertaut.  

## Gambar  

## Kode
Untuk mengindikasi kode dalam baris, cukup mengapit dengan tanda kutip pembuka tunggal `` ` ``. Sebagai contoh:

    Gunakan fungsi `printf()` untuk menampilkan.

Sebagai `HTML`, dibaca:  

    <p>Gunakan fungsi <code>printf()</code> untuk menampilkan.</p>

akan tampil sebagai:  
Gunakan fungsi `printf()` untuk menampilkan.  

Untuk menyisipkan tanda kutip pembuka tunggal pada kode, kita bisa menggunakan dua kali tanda kutip pembuka tunggal sebagai pengapit kode. Seperti ini:  

    Tanda kutip pembuka tunggal `` ` `` pengapit kode.

Akan diolah menjadi:

    <p>Tanda kutip pembuka tunggal <code>`</code> pengapit kode.</p>

## Kepala Judul
Ada dua gaya penulisan kepala (_header_) cara Markdown yaitu _setext_ dan _atx_.

Gaya _setext_ menulis kata, frasa, kalimat judul kepala tingkat pertama diikuti baris baru dengan tanda sama dengan `=`, atau baris baru dengan tanda kurang `-` untuk kepala tingkat ke-dua.  
Contoh, ditulis:  

    Ini Judul Utama
    ===============

    Judul Tingkat 2
    ---------------

Sedangkan gaya _atx_ menulis judul kepala dengan diawali tanda pagar `#` pada awal baris. Kepala bertingkat atau anak judul ditulis sejumlah tanda pagar. Dua tanda pagar `##` sebelum judul diartikan sebagai kepala tingkat dua, sampai enam tingkat anak judul. 

Contoh:  

    # Judul Utama

    ## Ini Judul Tingkat 2 ####

    ###### Judul Tingkat Enam

Sebagai pilihan gaya _atx_, kepala bisa ditutup dengan tanda pagar. Cara ini murni hiasan atau agar judul mudah disadari.

## Kutipan
Kutipan dalam Markdown ditulis dengan tanda lebih besar `>` pada awal baris sebelum kata, frasa, kalimat bahkan paragraf terkutip. Cara ini mirip seperti mengutip pada surel.  
Contoh menulis kutipan:  

    > Most people are other people. Their thoughts are someone else's opinions, their lives a mimicry, their passions a quotation.
    > <cite>Oscar Wilde</cite>

Tampil:  
> Most people are other people. Their thoughts are someone else's opinions, their lives a mimicry, their passions a quotation.
> <cite>Oscar Wilde</cite>

## Daftar
Pada `HTML` ada dua macam daftar populer yaitu; daftar tidak urut (*UL* :: _unordered list_) dan daftar berurut (*OL* :: _ordered list_).

### Daftar tidak urut
Daftar ini ditulis dengan awalan tanda bintang `*`, tanda tambah `+`, atau tanda kurang `-` pada awal baris.  
Contoh, ditulis:  

    * Apel
    * Pisang
    * Jambu
       * Jambu biji
       * Jambu air
    + Ini juga daftar
    - Ini juga

Tampil:  
* Apel
* Pisang
* Jambu
   * Jambu biji
   * Jambu air
+ Ini juga daftar
- Ini juga

### Daftar berurut
Daftar ini ditulis dengan angka, tanda titik `.`, dan diikuti spasi pada awal baris.  
Contoh, ditulis:  
```markdown
1. Daftar pertama
2. Daftar ke-dua
   1. Anak pertama daftar ke-dua
   2. Anak ke-dua daftar ke-dua
   3. Anak ke-tiga daftar ke-dua
3. Daftar ke-tiga
4. Daftar ke-empat
4. Daftar selanjutnya
```
Ditampilkan menjadi:
1. Daftar pertama
2. Daftar ke-dua
   1. Anak pertama daftar ke-dua
   2. Anak ke-dua daftar ke-dua
   3. Anak ke-tiga daftar ke-dua
3. Daftar ke-tiga
4. Daftar ke-empat
4. Daftar selanjutnya

## Tabel
Ditulis:  

    Buah|Warna
    ---|---
    Apel| merah
    Pisang|kuning
    Alpukat|hijau

Ditampilkan menjadi:  

Buah|Warna
---|---
Apel| merah
Pisang|kuning
Alpukat|hijau

Ditulis:  
```markdown
| Nomor | Buah    | Warna  | Harga | 
|------:|---------|:------:|------:| 
| 1     | Apel    | merah  | 36000 | 
| 2     | Pisang  | kuning | 15000 | 
| 3     | Naga    | ungu   | 24000 | 
| 4     | Alpukat | hijau  | 20000 | 
```
Ditampilkan menjadi:  

| Nomor | Buah    | Warna  | Harga | 
|------:|---------|:------:|------:| 
| 1     | Apel    | merah  | 36000 | 
| 2     | Pisang  | kuning | 15000 | 
| 3     | Naga    | ungu   | 24000 | 
| 4     | Alpukat | hijau  | 20000 | 