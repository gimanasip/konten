---
judul: Hugo *Shortcodes*
deskripsi: Potongan kode sederhana di dalam berkas konten untuk memanggil templat bawaan atau khusus.
---
## Shortcodes? Kode Pendek

*Shortcodes* dalam Hugo adalah potongan kode sederhana di dalam berkas konten yang akan di-render Hugo menggunakan templat yang telah ditetapkan sebelumnya.

## Memakai Shortcodes

Dalam berkas konten, *Shortcodes* dapat dipanggil dengan kode `{{%/* namashortcode argumen */%}}`. Argumen kode pendek dibatasi spasi, dan argumen dengan spasi internal harus diberi tanda kutip.

Beberapa *Shortcodes* menggunakan atau memerlukan penutup. Seperti HTML, kode pembuka dan penutup yang diikuti nama, dengan penutup yang diawali dengan garis miring.

Berikut adalah dua contoh kode pendek berpasangan:

```
{{%/* mdshortcode */%}}Stuff to `process` in the *center*.{{%/* /mdshortcode */%}}
```
```
{{</* highlight go */>}} A bunch of code here {{</* /highlight */>}}
```

## Shortcodes Bawaan

### figure
### gist
### highlight
### instagram
### param
### ref
### relref
### twitter
### vimeo
### youtube

## Shortcodes Khusus

### google/sheets
### hitung/laman

{{< site/title >}} berisi {{< hitung/laman_biasa >}} laman biasa dari total {{< hitung/laman_semua >}} laman.

