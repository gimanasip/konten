---
judul: "Shortcodes: Google Sheets"
deskripsi: "*shortcodes `google/sheets`*"
---

## sheetrock

### Mencoba

#### `test/parse-x`

```
{{</* test/parse-x "https://docs.google.com/spreadsheets/d/1U50fBqQTVqYl2Ja7ZfQ3FjKYy7Gi1h8PH_6cPZWvnDk/edit#gid=0" */>}}
```

{{< test/parse-x "https://docs.google.com/spreadsheets/d/1U50fBqQTVqYl2Ja7ZfQ3FjKYy7Gi1h8PH_6cPZWvnDk/edit#gid=0" >}}

#### `google/sheets`

```
{{</* google/sheets id="table1" url="https://docs.google.com/spreadsheets/d/1U50fBqQTVqYl2Ja7ZfQ3FjKYy7Gi1h8PH_6cPZWvnDk/edit#gid=0" query="select A,B,C,D,E,F" */>}}
```

{{< google/sheets id="table1" url="https://docs.google.com/spreadsheets/d/1U50fBqQTVqYl2Ja7ZfQ3FjKYy7Gi1h8PH_6cPZWvnDk/edit#gid=0" query="select A,B,C,D,E,F" >}}

### Mencoba *sheet:2* `piano`

#### `test/parse-x`

```
{{</* test/parse-x "https://docs.google.com/spreadsheets/d/1nw0Rfe_nnBSj5ppMXzTAIQoOkIMgNKCY7mrYYjRrcqY/edit#gid=624758352" */>}}
```

{{< test/parse-x "https://docs.google.com/spreadsheets/d/1nw0Rfe_nnBSj5ppMXzTAIQoOkIMgNKCY7mrYYjRrcqY/edit#gid=624758352" >}}

#### `google/sheets`

```
{{</* google/sheets id="piano" url="https://docs.google.com/spreadsheets/d/1nw0Rfe_nnBSj5ppMXzTAIQoOkIMgNKCY7mrYYjRrcqY/edit#gid=624758352" query="select D,E,A,B,C" */>}}
```

{{< google/sheets id="piano" url="https://docs.google.com/spreadsheets/d/1nw0Rfe_nnBSj5ppMXzTAIQoOkIMgNKCY7mrYYjRrcqY/edit#gid=624758352" query="select D,E,A,B,C" >}}

### Mencoba *sheet:3* `chromaperc`

#### `test/parse-x`

```
{{</* test/parse-x "https://docs.google.com/spreadsheets/d/1nw0Rfe_nnBSj5ppMXzTAIQoOkIMgNKCY7mrYYjRrcqY/edit#gid=2047716750" */>}}
```

{{< test/parse-x "https://docs.google.com/spreadsheets/d/1nw0Rfe_nnBSj5ppMXzTAIQoOkIMgNKCY7mrYYjRrcqY/edit#gid=2047716750" >}}

#### `google/sheets`

```
{{</* google/sheets id="chromaperc" url="https://docs.google.com/spreadsheets/d/1nw0Rfe_nnBSj5ppMXzTAIQoOkIMgNKCY7mrYYjRrcqY/edit#gid=2047716750" query="select D,E,A,B,C" */>}}
```

{{< google/sheets id="chromaperc" url="https://docs.google.com/spreadsheets/d/1nw0Rfe_nnBSj5ppMXzTAIQoOkIMgNKCY7mrYYjRrcqY/edit#gid=2047716750" query="select D,E,A,B,C" >}}

#### `test/feeds-g sheets`

```
{{</* test/feeds-gsheets id="midi-piano" url=https://spreadsheets.google.com/feeds/cells/1nw0Rfe_nnBSj5ppMXzTAIQoOkIMgNKCY7mrYYjRrcqY/2/public/values?alt=json-in-script&callback=doData" */>}}
```

{{< test/feeds-gsheets id="midi-piano" url=https://spreadsheets.google.com/feeds/cells/1nw0Rfe_nnBSj5ppMXzTAIQoOkIMgNKCY7mrYYjRrcqY/2/public/values?alt=json-in-script&callback=doData" >}}

### Mencoba *Daftar Kustom*

#### `google/sheets/daftar-cells`

```
{{</* google/sheets/daftar-cells id="daftarku" url="https://docs.google.com/spreadsheets/d/1U50fBqQTVqYl2Ja7ZfQ3FjKYy7Gi1h8PH_6cPZWvnDk/edit#gid=0" cells="{{calls.jumlah}} {{calls.Buah}} <strong>{{calls.harga}}</strong>" */>}}
```

{{< google/sheets/daftar-cells id="daftarku" url="https://docs.google.com/spreadsheets/d/1U50fBqQTVqYl2Ja7ZfQ3FjKYy7Gi1h8PH_6cPZWvnDk/edit#gid=0" cells="{{cells.jumlah}} {{cells.Buah}}, <strong>{{cells.harga}}</strong>" >}}

### Mencoba *Daftar Deskripsi* `glosarium`

#### `(google/sheets/)glosarium`

```
{{</* glosarium id="glosarium1" url="https://docs.google.com/spreadsheets/d/1U50fBqQTVqYl2Ja7ZfQ3FjKYy7Gi1h8PH_6cPZWvnDk/edit#gid=0" */>}}
```

{{< glosarium id="glosarium1" url="https://docs.google.com/spreadsheets/d/1U50fBqQTVqYl2Ja7ZfQ3FjKYy7Gi1h8PH_6cPZWvnDk/edit#gid=0" >}}