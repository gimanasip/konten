---
judul: "Bentuk"  
draft: true  
---

### Papan Ketik `<kbd>` {.x}
Ditulis:  

    [ctrl](:key) + [alt](:kbd) + [del](:k)  

Tampil:  
[ctrl](:key)+[alt](:kbd)+[del](:k)

{{< x >}}

### *Superscript* `<sup>` {.x}
`cm[2](^sup)` cm[2](^sup)

### *Subscript* `<sub>`
`H[2](^sub)O` H[2](^sub)O

{{< x >}}
### Besar `<big>` {.x}
Ditulis:  

    Ini adalah [kata](:big) yang dicetak [besar](:besar).

Tampil:  
Ini adalah [kata](:big) yang dicetak [besar](:besar).

### Kecil `<small>` 
Ditulis:  

    Ini adalah [kata](:small) yang dicetak [kecil](:kecil).

Tampil:  
Ini adalah [kata](:small) yang dicetak [kecil](:kecil).

{{< x >}}
### Sorotan `<mark>` {.x}
Ditulis:  

    Ini adalah [kata](:soroti) yang [disoroti](:mark).

Tampil:  
Ini adalah [kata](:soroti) yang disoroti.

{{< x >}}
### `<ruby>`  

```html  
<ruby>
  漢 <rp>(</rp><rt>Kan</rt><rp>)</rp>
  字 <rp>(</rp><rt>ji</rt><rp>)</rp>
</ruby>
```

Tampil:  
<ruby>
  漢 <rp>(</rp><rt>Kan</rt><rp>)</rp>
  字 <rp>(</rp><rt>ji</rt><rp>)</rp>
</ruby>