---
judul: Nomor Halaman
---

`<hr>` `role="doc-pagebreak"`

`![halaman](001)`
![halaman](001)

`![hlm.](002)`
![hlm.](002)

`![hal.](034)`
![hal.](034)

`[laman](234 berdasarkan naskah sumber cetak terpindai perpustakaan")`
![laman](234 "berdasarkan naskah sumber cetak terpindai perpustakaan")

`![halaman](1-2)`
![halaman](1-2)