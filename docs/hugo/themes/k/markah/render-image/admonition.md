## Kotak Peringatan atau *Admonition*
| tingkat | warna          | status  | ikon            | alt              |
| ------- | -------------- | ------- | --------------- | ---------------- |
| 1       | [abu](!)       | biasa   | -               | notif            |
| 2       | [biru](!!)     | aktif   | [:ikon:](info)  | info             |
| 3       | [hijau](!!!)   | waspada | [:ikon:](betul) | penting          |
| 4       | [kuning](!!!!) | siaga   | [:ikon:](awas)  | peringatan       |
| 5       | [merah](!!!!!) | awas    | [:ikon:](salah) | dilarang, bahaya |

### Tingkat 1 `!`

`![ini adalah tulisan *admonition*](!apapun "Judul")`

![ini adalah tulisan *admonition*](!apapun "Judul")

### Tingkat 1: Aktif (biru) `!!`
+ tip
+ info

`![ini adalah isi **informasi** yang panjang untuk mencoba ganti baris pada kotak informasi.](!!info "Judul Informasi")`  

Paragraf sebelum kotak informasi.
![ini adalah isi **informasi** yang panjang untuk mencoba ganti baris pada kotak informasi.](!!info "Judul Informasi")
Paragraf sesudah kotak informasi.  

### Tingkat 2: Waspada (hijau) `!!!`

`![ini adalah tulisan penting](!!!penting "Judul Penting")`

![ini adalah tulisan penting](!!!penting: "Judul Penting")

### Tingkat 3: Siaga (kinung) `!!!!`

`![ini adalah isi peringatan](!!!!ingat.hero.anti "Judul Peringatan")`

![ini adalah isi peringatan](!!!!ingat.hero.anti "Judul Peringatan")

### Tingkat 4: Awas (merah) `!!!!!`

`![ini adalah pesan awas](!!!!!awas "Judul Awas")`

![ini adalah pesan awas menggunakan `<div>`](!!!!!awas "Judul Awas")