---
Judul: "Markdown: Pandu Lanjutan"
deskripsi: "Cara menulis dan teknik lanjutan menggunakan Markdown"
warna: "#888"
draft: true
tutorial:
  - Markdown
---

Markdown lanjutan ini merupakan markah yang dirakit agar memenuhi kebutuhan menulis. Gaya yang dipakai mungkin belum terlalu umum di kalangan luas.

## Catatan Kaki 
Contoh penulisan catatan kaki:  

    Catatan kaki [^1]

    [^1]: Catatan kaki adalah catatan yang berada di bagian bawah dari halaman karya ilmiah. Fungsi catatan kaki adalah sebagai tempat pencantuman identitas sumber rujukan dari pengutipan informasi di bagian badan teks atau badan paragraf.

Tampil:  
Catatan kaki [^1]

[^1]: Catatan kaki adalah catatan yang berada di bagian bawah dari halaman karya ilmiah. Fungsi catatan kaki adalah sebagai tempat pencantuman identitas sumber rujukan dari pengutipan informasi di bagian badan teks atau badan paragraf.

## Daftar Definisi 
Setiap pasang terminologi dan definisi dipisah menggunakan baris kosong.  
Contoh penulisan:

    dt
    : dd

    Term
    : Describe

    Istilah
    : Penjelasan

    Tanya
    : Jawab

Diproses ke HTML menjadi:  
```html
<dl>
  <dt>dt</dt>
  <dd>dd</dd>
  <dt>Term</dt>
  <dd>Describe</dd>
  <dt>Istilah</dt>
  <dd>Penjelasan</dd>
  <dt>Tanya</dt>
  <dd>Jawab</dd>
</dl>
```

Ditampilkan seperti di bawah ini:  

dt
: dd

Term
: Describe

Istilah
: Penjelasan

Tanya
: Jawab
