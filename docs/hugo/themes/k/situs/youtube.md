---
judul: "YouTube"
draft: true
---

+ [Embed video and playlist](https://www.youtube.com/watch?v=lJIrF4YjHfQ)
+ Short URL `youtu.be/`

### User & Channel {.x}

`youtube.com/user/YouTubeHelp`

`youtube.com/channel/UCMDQxm7cUx3yXkfeHa5zJIQ`

{{< x >}}
### Video {.x}

`https://www.youtube.com/watch?v=lJIrF4YjHfQ`

#### sematan {.x}
```html
<iframe width="560" height="315" src="https://www.youtube.com/embed/lJIrF4YjHfQ" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
```

`![Youtube](https://www.youtube.com/watch?v=lJIrF4YjHfQ)`

![Youtube](https://www.youtube.com/watch?v=lJIrF4YjHfQ)

{{< x >}}
#### dengan takarir {.x}
Sematan menyertakan *Title* sebagai takarir.  
`![Embed videos and playlists](youtube.com/embed/lJIrF4YjHfQ "Tutorial:")`

![Embed videos and playlists](youtube.com/embed/lJIrF4YjHfQ "Tutorial:")

{{< x >}}
#### tautan pendek {.x}

`![Youtube](youtu.be/lJIrF4YjHfQ)`

![Youtube](youtu.be/lJIrF4YjHfQ)

{{< x >}}
#### tanpa kuki {.x}
`![Youtube](youtube-nocookie.com/watch?v=UsFCsRbYDyA)`

![Youtube](youtube-nocookie.com/watch?v=UsFCsRbYDyA)

{{< x >}}
#### sematan umpan {.x}
*Title* diawali tanda `$`, maka sematan akan mengumpan ke situs YouTube.  
`![Judul Sematan YT-Hook](youtube-nocookie.com/watch?v=UsFCsRbYDyA "$halo")`

![Judul Sematan YT-Hook](youtube-nocookie.com/watch?v=UsFCsRbYDyA "$halo")

{{< x >}}
{{< x >}}
### *Playlist* & seri {.x}

`https://www.youtube.com/playlist?list=PLx0sYbCqOb8TBPRdmBHs5Iftvv9TPboYG`

```html
<iframe width="560" height="315" src="https://www.youtube.com/embed/videoseries?list=PLx0sYbCqOb8TBPRdmBHs5Iftvv9TPboYG" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
```

*Playlist*: `PLx0sYbCqOb8TBPRdmBHs5Iftvv9TPboYG`

`![Youtube](youtube.com/playlist?list=PLx0sYbCqOb8TBPRdmBHs5Iftvv9TPboYG&feature=share&playnext=1)`

![Youtube](youtube.com/playlist?list=PLx0sYbCqOb8TBPRdmBHs5Iftvv9TPboYG&feature=share&playnext=1)

`![Youtube](https://www.youtube.com/embed/videoseries?list=PLx0sYbCqOb8TBPRdmBHs5Iftvv9TPboYG)`

![Youtube](https://www.youtube.com/embed/videoseries?list=PLx0sYbCqOb8TBPRdmBHs5Iftvv9TPboYG)

{{< x >}}
### Keluku {.x}
Tautan keluku atau *thumbnail* YouTube.  
Dari [Asaph dan Peter Mortensen](https://stackoverflow.com/questions/2068344/how-do-i-get-a-youtube-video-thumbnail-from-the-youtube-api).
```
https://img.youtube.com/vi/<insert-youtube-video-id-here>/0.jpg
https://img.youtube.com/vi/<insert-youtube-video-id-here>/1.jpg
https://img.youtube.com/vi/<insert-youtube-video-id-here>/2.jpg
https://img.youtube.com/vi/<insert-youtube-video-id-here>/3.jpg
```
Pertama `0.jpg` adalah ukuran penuh. Keluku bawaan (bisa `1.jpg`, `2.jpg`, atau `3.jpg`) tertaut sebagai:
```
https://img.youtube.com/vi/<insert-youtube-video-id-here>/default.jpg
```
tautan keluku *high quality*:
```
https://img.youtube.com/vi/<insert-youtube-video-id-here>/hqdefault.jpg
```
tautan keluku *medium quality*:
```
https://img.youtube.com/vi/<insert-youtube-video-id-here>/mqdefault.jpg
```
tautan keluku *standard definition*:
```
https://img.youtube.com/vi/<insert-youtube-video-id-here>/sddefault.jpg
```
tautan keluku *maximum resolution*:
```
https://img.youtube.com/vi/<insert-youtube-video-id-here>/maxresdefault.jpg
```
Tautan juga tersedia dalam HTTP, juga nama penyedia `i3.ytimg.com` sebagai pengganti tetaut `img.youtube.com` di atas.

Sebagai alternatif juga bisa menggunakan [YouTube Data API (v3)](https://developers.google.com/youtube/v3/)[^YTAPI3].

[^oEmbed]: You can query the Instagram oEmbed endpoint to get an Instagram post’s embed HTML and basic metadata in order to display the post in another website or app. Supports photo, video, Reel, and Guide posts.
[^YTAPI3]: YouTube Data [API](https://developers.google.com/youtube/v3/) (v3) https://developers.google.com/youtube/v3/

{{< x >}}