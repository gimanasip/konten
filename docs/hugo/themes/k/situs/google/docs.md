---
judul: "Google Docs"
---

### Google Docs {.x}

https://docs.google.com/document/d/1xRWBHwv5yp27QFWfivRBbRcSwkRezIMy8uJF92ARI1s/edit?usp=sharing

+ Share edit URL `https://docs.google.com/document/d/1xRWBHwv5yp27QFWfivRBbRcSwkRezIMy8uJF92ARI1s/edit?usp=sharing`
+ Public URL `https://docs.google.com/document/d/e/2PACX-1vSmvJlgPwRe_7WcJdTUJm4ymfIwLS2wM_kTDY8NuuPinwk5236NtUdxOqTqeryb4otCzPqnoYbGDXFz/pub`

```html
<iframe src="https://docs.google.com/document/d/e/2PACX-1vSmvJlgPwRe_7WcJdTUJm4ymfIwLS2wM_kTDY8NuuPinwk5236NtUdxOqTqeryb4otCzPqnoYbGDXFz/pub?embedded=true"></iframe>
```

Direct download format  
+ docx
+ odt
+ rtf
+ pdf
+ txt
+ html
+ epub

```
https://docs.google.com/document/d/DOC_FILE_ID/export?format=pdf
https://docs.google.com/document/d/DOC_FILE_ID/export?format=doc
```

{{< x >}}
### Google Sheets {.x}
+ Share edit URL `https://docs.google.com/spreadsheets/d/1D9AWGaMbsfnRKahPzZvv3phn8J46y3FiJu0OXAVeZsM/edit?usp=sharing`
+ Public URL webpage `https://docs.google.com/spreadsheets/d/e/2PACX-1vSSl0Az_wr87wc474EbDQQkBPQH0LQzs47ElRtSsc-djVor5W0J32o1V3VEr06MsAUekerxCRjP0s88/pubhtml`

+ Public URL output format
  + `/pubhtml`
  + `/pub?output=csv`
  + `/pub?output=tsv`
  + `/pub?output=xlsx`
  + `/pub?output=pdf`
  + `/pub?output=ods`

Direct download format  
+ xlsx
+ ods
+ pdf
+ html
+ `csv?pageid=pPAGE_NUMBER`
+ `tsv?pageid=pPAGE_NUMBER`

```
https://docs.google.com/spreadsheets/d/FILE_ID/export?format=xlsx
https://docs.google.com/spreadsheets/d/FILE_ID/export?format=pdf
https://docs.google.com/spreadsheets/d/FILE_ID/export?format=csv
```

#### CSV menjadi tabel

```
https://docs.google.com/spreadsheets/d/1D9AWGaMbsfnRKahPzZvv3phn8J46y3FiJu0OXAVeZsM/export?gid=0&format=csv
```

![Tulisan ini akan menjadi rangkuman tabel dari Google Sheets](https://docs.google.com/spreadsheets/d/1D9AWGaMbsfnRKahPzZvv3phn8J46y3FiJu0OXAVeZsM/export?gid=0&format=csv "Ini adalah takarir dari table")

+ Share Sheet  
```
https://docs.google.com/spreadsheets/d/1AzwTG1Irsryf_aGAcuwdEma550nU9m27JxGGasLCoCQ/edit#gid=0
```

```
https://docs.google.com/spreadsheets/d/e/2PACX-1vSSl0Az_wr87wc474EbDQQkBPQH0LQzs47ElRtSsc-djVor5W0J32o1V3VEr06MsAUekerxCRjP0s88/pubhtml
```

```html
<iframe src="https://docs.google.com/spreadsheets/d/e/2PACX-1vSSl0Az_wr87wc474EbDQQkBPQH0LQzs47ElRtSsc-djVor5W0J32o1V3VEr06MsAUekerxCRjP0s88/pubhtml?widget=true&amp;headers=false"></iframe>
```

![Tulisan ini akan menjadi rangkuman tabel dari Google Sheets](https://docs.google.com/spreadsheets/d/1AzwTG1Irsryf_aGAcuwdEma550nU9m27JxGGasLCoCQ/edit#gid=0 ":tabel: Ini adalah takarir dari table Google Sheets")

{{< x >}}
### Google Slides {.x}
+ Share edit URL `https://docs.google.com/presentation/d/1CzdoIQc9Vgg5J6IMIonpTcV41p_R4B5xccgzJjfilYM/edit?usp=sharing`
+ Public URL `https://docs.google.com/presentation/d/e/2PACX-1vTT2gFSANgwLogjhf-fvaKv_lvwwT8Nma_oOCbuO4V9roPOLa9SxeavcnOM0fesknN2uL1yo7gOTm9d/pub?start=false&loop=false&delayms=3000`  

```html
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vTT2gFSANgwLogjhf-fvaKv_lvwwT8Nma_oOCbuO4V9roPOLa9SxeavcnOM0fesknN2uL1yo7gOTm9d/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
```

Direct download format  
+ pptx
+ odp
+ pdf
+ txt 
+ `jpg?pageid=pPAGE_NUMBER`
+ `png?pageid=pPAGE_NUMBER`
+ `svg?pageid=pPAGE_NUMBER`

```
https://docs.google.com/presentation/d/PRESENTATION_ID/export/pdf
https://docs.google.com/presentation/d/PRESENTATION_ID/export/pptx
```

{{< x >}}
### Google Forms {.x}
+ Share edit URL `https://docs.google.com/forms/d/15A-4juDNFFra5aDHZCMCp7tn4f7irGdq9MVmdN8bfXs/edit?usp=sharing`
+ Participant URL `https://docs.google.com/forms/d/e/1FAIpQLSe7ixYJH1h04-CQACQhYkLWnGi1ZUjsUQZ7nGNrCkRIZlyvhA/viewform?usp=sf_link`

```html
<iframe src="https://docs.google.com/forms/d/e/1FAIpQLSe7ixYJH1h04-CQACQhYkLWnGi1ZUjsUQZ7nGNrCkRIZlyvhA/viewform?embedded=true" width="640" height="366" frameborder="0" marginheight="0" marginwidth="0">Memuat…</iframe>
```

{{< x >}}
### Google Drawings {.x}
`https://docs.google.com/drawings/d/FILE_ID/edit`

```
https://docs.google.com/drawings/d/FILE_ID/export/svg
https://docs.google.com/drawings/d/FILE_ID/export/pdf
https://docs.google.com/drawings/d/FILE_ID/export/jpg
```

```html
<p>
  <img src="https://docs.google.com/drawings/d/FILE_ID/export/png" alt="Google Drawing" />
</p>
```

{{< x >}}
## gdoc.pub
+ doc `https://gdoc.pub/doc/e/2PACX-1vSmvJlgPwRe_7WcJdTUJm4ymfIwLS2wM_kTDY8NuuPinwk5236NtUdxOqTqeryb4otCzPqnoYbGDXFz`
+ sheet `https://gdoc.pub/sheet/e/2PACX-1vSSl0Az_wr87wc474EbDQQkBPQH0LQzs47ElRtSsc-djVor5W0J32o1V3VEr06MsAUekerxCRjP0s88`

## GFormX