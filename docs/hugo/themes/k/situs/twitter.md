---
judul: "Twitter"
draft: true
---

Penyematan dari [Twitter Publish](https://publish.twitter.com/)

### Collection {.x}
`https://twitter.com/TwitterDev/timelines/539487832448843776`

```html
<a class="twitter-timeline" href="https://twitter.com/TwitterDev/timelines/539487832448843776?ref_src=twsrc%5Etfw">National Park Tweets - Curated tweets by TwitterDev</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
```
{{< x >}}
### Tweet {.x}
`https://twitter.com/Twitter/status/1319006269785755648`

```html
<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Reading an article before Retweeting it? That’s growth.<br><br>Before you Retweet an article, we’ll remind you to read it first. <a href="https://t.co/oVyTmSCc7O">pic.twitter.com/oVyTmSCc7O</a></p>&mdash; Twitter (@Twitter) <a href="https://twitter.com/Twitter/status/1319006269785755648?ref_src=twsrc%5Etfw">October 21, 2020</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
```

`![Reading an article before Retweeting it? That’s growth. Before you Retweet an article, we’ll remind you to read it first.](https://twitter.com/Twitter/status/1319006269785755648 "Twitter")`

![Reading an article before Retweeting it? That’s growth. Before you Retweet an article, we’ll remind you to read it first.](https://twitter.com/Twitter/status/1319006269785755648 "Twitter")

{{< x >}}
### Profile {.x}

Timeline atau tombol (follow atau mention).

`https://twitter.com/TwitterDev`

+ timeline
```html
<a class="twitter-timeline" href="https://twitter.com/TwitterDev?ref_src=twsrc%5Etfw">Tweets by TwitterDev</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
```

{{< x >}}
### List {.x}
`https://twitter.com/TwitterDev/lists/national-parks`

```html 
<a class="twitter-timeline" href="https://twitter.com/TwitterDev/lists/national-parks?ref_src=twsrc%5Etfw">A Twitter List by TwitterDev</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
```

{{< x >}}
### Moments (now Events) {.x}
`https://twitter.com/i/moments/625792726546558977`

```html
<a class="twitter-moment" href="https://twitter.com/i/moments/625792726546558977?ref_src=twsrc%5Etfw">Michelle Obama Opens Special Olympics</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
```

{{< x >}}
### Likes timeline {.x}
`https://twitter.com/TwitterDev/likes`

```html 
<a class="twitter-timeline" href="https://twitter.com/TwitterDev/likes?ref_src=twsrc%5Etfw">Tweets Liked by @TwitterDev</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
```

{{< x >}}
### Handle {.x}

Timeline atau tombol (follow, mention, atau hashtag).

`@TwitterDev`

```html 
<a class="twitter-timeline" href="https://twitter.com/TwitterDev?ref_src=twsrc%5Etfw">Tweets by TwitterDev</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
```

{{< x >}}
### Hashtag {.x}

`#LoveTwitter`

```html 
<a href="https://twitter.com/intent/tweet?button_hashtag=LoveTwitter&ref_src=twsrc%5Etfw" class="twitter-hashtag-button" data-size="large" data-text="this is tweet" data-url="https://twitter.com" data-related="twitter,TwitterDev" data-lang="id" data-show-count="false">Tweet #LoveTwitter</a><script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
```

{{< x >}}
  
