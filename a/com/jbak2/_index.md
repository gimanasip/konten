---
judul: "Jbak2 Keyboard"
deskripsi: "Aplikasi Android untuk merakit *keyboard*. Mengatak papan ketik dengan banyak kemampuan setelan dan fungsi, tombol *kustom*, emoji, *clipboard* dan *template* cepat, karakter spesial, kalkulator, pintasan yang dapat disesuaikan dengan kebutuhan."
os: 
  - Android
aplikasi: 
  - "Papan Ketik"
  - Keyboard
android: 
  - Keyboard
draft: true
---

**Jbak2** adalah aplikasi *keyboard* juga sebagai alat perakit atak maupun kosmetik *keyboard* di layar Android.

Pada awal pemasangan, tersedia beberapa atak dan *skin* bawaan. Atak dan tema bawaan bisa dimodifikasi atau membuat baru. Jadi, kita bisa merakit *keyboard* sesuai kebutuhan mengetik. 

Tombol besar atau kecil. Atak lengkap atau sederhana dapat disesuaikan. *Itu bisa diatur, Don*.

Berikut cara *install* dan setelan dasar. Cara mengatak dibahas pada bagian selanjutnya.

## Pemasangan
Cara memasang Jbak2 seperti pemasangan aplikasi *keyboard* pada umumnya.
1. Pasang dari [Play Store](https://play.google.com/store/apps/details?id=com.jbak2.JbakKeyboard)
2. Nyalakan di **Setelan** ➝ **Bahasa & Masukan** ➝ **Atur Keyboard** ➝ nyalakan saklar Jbak2 Keyboard. 
3. Aktifkan pada posisi mengetik, melalui notifikasi ➝ **Ubah keyboard** ➝ **Jbak2 Keyboard** ➝ **Pilih keyboard**.

## Setelan Dasar
Pengaturan bisa diakses melalui ikon aplikasi Jbak2 keyboard, atau pada *keyboard* sendiri; menahan tombol [opt](:k) ➝ pilih ***Jbak2 keyboard setting***.

Pada setelan Jbak2 terdapat beberapa pilihan.

+ *Quick setting wizard* [^1]
+ *Application ratting* [^2]
+ *How to use the keyboard* [^3]
+ *Dictionaries* [^4]
+ *Appearance* [^5]
+ *Key setting* [^6]
+ *Languages and layouts* [^7]
+ *Custom languages and layouts* [^8]
+ *Autocomplete* [^9]
+ *Autoinput* [^10]
+ *Small keyboard* [^11]
+ *Main menu display* [^12]
+ *Display key preview* [^13]
+ *Vibration and sound* [^14]
+ *Gestures* [^15]
+ *Other settings* [^16]
+ *Other* [^17]

[^1]: digunakan untuk setelan cepat.
[^2]: Beri skor di Play Store, Михаил Вязенкин
[^3]: Panduan dalam bahasa Inggris dan Russia dari [situs pengembang Jbak2](https://jbak2.ucoz.net)
[^4]: Setelan kamus untuk pelengkap otomatis. Juga berisi pilihan aplikasi ([jbak2dict](https://play.google.com/store/apps/details?id=com.jbak2.dictionary)) pengunduh kamus.
[^5]: Setelan tampilan *keyboard* berisi *skin*, fon, ukuran. Juga berisi pilihan aplikasi ([jbak2skin](https://play.google.com/store/apps/details?id=com.jbak2.skin)) pengunduh *skin*.
[^6]: Setelan ukuran tinggi dan koreksi letak keyboard.
[^7]: Pemilihan atak untuk setiap bahasa. 
[^8]: Berisi pilihan aplikasi ([Jbak2layout](https://play.google.com/store/apps/details?id=com.jbak2.layout)) pengunduh atak.
[^9]: Setelan tombol pelengkap kata otomatis.
[^10]: Setelan koreksi dan masukan otomatis lainnya.
[^11]: Setelan *keyboard* kecil (masih dalam tahap beta)
[^12]: Setelan pilihan yang ditampilkan pada tombol [opt](:k).
[^13]: Setelan munculan tombol terketuk.
[^14]: Setelan getar dan suara tombol terketuk.
[^15]: Setelan gestur atau gerak usap.
[^16]: Setelan lain seperti *clipboard*, templat, tombol volume.
[^17]: Informasi, kontak pengembang, dan saran aplikasi.

Banyak setelan...

## Rumit?

> Simplicity is the Ultimate Sophistication 
>  
> Leonardo da Vinci

Dari kompleksitas Jbak2 bisa dibikin *keyboard* untuk bahasa daerah dan asing, bahasa pemrograman, *chatting* secara sangkil, bikin takarir cepat, *nulis* konten secara gesit, bahkan untuk kemampuan khusus.