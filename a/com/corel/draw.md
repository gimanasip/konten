---
judul: "CorelDRAW"
deskripsi: "Perangkat lunak pengolah grafis vektor."
os: 
  - Windows
  - MacOS
aplikasi: 
  - Vektor
  - Grafika
warna: "#5ca542"
---