---
judul: "Google Docs"
deskripsi: "Situs aplikasi pembuatan dokumen dan pengolahan kata."
os: 
  - "Web App"
aplikasi: 
  - Dokumen
  - Kantor
  - Teks
warna: "#1e88e5"
---