---
judul: "Google Sheets"
deskripsi: "Situs aplikasi pembuatan tabel dan pengolahan data."
os: 
  - "Web App"
aplikasi: 
  - Dokumen
  - Kantor
  - Tabel
warna: "#4caf50"
---