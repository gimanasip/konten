---
judul: "Inkscape"
deskripsi: "Perangkat lunak pengolah grafis vektor."
os: 
  - Linux
  - Windows
  - MacOS
aplikasi: 
  - Vektor
  - Grafika
warna: "#404040"
---