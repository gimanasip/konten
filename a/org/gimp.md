---
judul: "GIMP"
deskripsi: "Perangkat lunak pengolah grafis dan citra foto."
os: 
  - Linux
  - Windows
  - MacOS
aplikasi: 
  - Foto
  - Grafika
warna: "#ae8b7b"
---