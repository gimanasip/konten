---
judul: "Audacity"
deskripsi: "Perangkat lunak perekam dan pengolah bunyi."
os: 
  - Linux
  - Windows
  - MacOS
aplikasi: 
  - Audio
  - Rekam
warna: "#f47b17"
---