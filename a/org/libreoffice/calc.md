---
judul: "LibreOffice Calc"
deskripsi: "Perangkat lunak pembuatan tabel dan pengolahan data."
os: 
  - Linux
  - Windows
  - MacOS
aplikasi: 
  - Dokumen
  - Kantor
  - Tabel
warna: "#31b81d"
---