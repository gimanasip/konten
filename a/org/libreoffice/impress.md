---
judul: "LibreOffice Impress"
deskripsi: "Perangkat lunak presentasi slide."
os: 
  - Linux
  - Windows
  - MacOS
aplikasi: 
  - Dokumen
  - Kantor
  - Presentasi
warna: "#c25411"
---