---
judul: "OpenShot"
deskripsi: "Perangkat lunak penyunting video."
os: 
  - Linux
  - Windows
  - MacOS
aplikasi: 
  - Video
warna: "#4e8bc2"
---